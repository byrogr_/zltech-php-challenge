<?php

namespace Tests;

use App\Mobile;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class MobileTest extends TestCase
{

    /** @test */
    public function it_returns_null_when_name_empty()
    {
        $provider = \Mockery::mock('App\Interfaces\CarrierInterface');
        $mobile = new Mobile($provider);

        $this->assertNull($mobile->makeCallByName(''));
    }

}
